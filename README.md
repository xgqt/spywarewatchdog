# Spyware Watchdog Mirror

Spyware Watchdog Mirror on GitLab Pages.
Powered by '.gitlab-ci.yml' and of course GitLab Pages system.

This is probably as simple as it gets.
Git submodule as 'public'.

Artifacts last around 3 weeks since the last mirror.
